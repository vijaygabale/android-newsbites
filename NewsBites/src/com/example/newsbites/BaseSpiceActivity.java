package com.example.newsbites;

import com.octo.android.robospice.*;

import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.JacksonGoogleHttpClientSpiceService;


import android.app.Activity;

public class BaseSpiceActivity extends Activity {
	private SpiceManager spiceManager = new SpiceManager(JacksonGoogleHttpClientSpiceService.class);

	@Override
	protected void onStart() {
		spiceManager.start(this);
		super.onStart();
	}

	@Override
	protected void onStop() {
		spiceManager.shouldStop();
		super.onStop();
	}

	protected SpiceManager getSpiceManager() {
		return spiceManager;
	}

}
