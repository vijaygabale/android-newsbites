package com.example.newsbites.ui;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.newsbites.R;

public class ArticleInfo extends ArrayAdapter<Integer>{
	
	private final Activity context;	
	private final Integer[] count;
	private final Integer[] imageId;
	
	public ArticleInfo(Activity context, Integer[] count, Integer[] imageId) {
		super(context, R.layout.list_single);
		this.context = context;
		this.count = count;
		this.imageId = imageId;
	}
	
	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView= inflater.inflate(R.layout.list_single, null, true);
		
		TextView facebookCount = (TextView) rowView.findViewById(R.id.facebookCount);
		ImageView facebookIcon = (ImageView) rowView.findViewById(R.id.facebookIcon);
		facebookCount.setText(count[0]);
		facebookIcon.setImageResource(imageId[0]);
		
		TextView twitterCount = (TextView) rowView.findViewById(R.id.twitterCount);
		ImageView twitterIcon = (ImageView) rowView.findViewById(R.id.twitterIcon);
		twitterCount.setText(count[1]);
		twitterIcon.setImageResource(imageId[1]);
		
		TextView linkedInCount = (TextView) rowView.findViewById(R.id.linkedInCount);
		ImageView linkedInIcon = (ImageView) rowView.findViewById(R.id.linkedInIcon);
		linkedInCount.setText(count[2]);
		linkedInIcon.setImageResource(imageId[2]);
		
		TextView gplusCount = (TextView) rowView.findViewById(R.id.gplusCount);
		ImageView gplusIcon = (ImageView) rowView.findViewById(R.id.gplusIcon);
		gplusCount.setText(count[3]);
		gplusIcon.setImageResource(imageId[3]);
		
		TextView commentCount = (TextView) rowView.findViewById(R.id.commentsCount);
		ImageView commentIcon = (ImageView) rowView.findViewById(R.id.commentsIcon);
		commentCount.setText(count[4]);
		commentIcon.setImageResource(imageId[4]);
		
		
		return rowView;
	}
}