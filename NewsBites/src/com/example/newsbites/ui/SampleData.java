package com.example.newsbites.ui;

import java.util.ArrayList;
import java.util.Random;

public class SampleData {

	public static final int SAMPLE_DATA_ITEM_COUNT = 10;

public static ArrayList<Data> generateSampleData() {
	
	String repeat = " repeat";
	
	final ArrayList<Data> datas = new ArrayList<Data>();
	
	for (int i = 0; i < SAMPLE_DATA_ITEM_COUNT; i++) {
		Data data = new Data();
		data.imageUrl = "http://blogs.uoregon.edu/eric/files/2014/03/technology2-vunqs5.jpg";
		data.title = "Pinterest Card";
		data.description = "Super awesome description";
		Random ran = new Random();
		int x = ran.nextInt(i + SAMPLE_DATA_ITEM_COUNT); 
		for (int j = 0; j < x; j++)
			data.description += repeat;
		datas.add(data);
	}
	
	return datas;
	
}
}