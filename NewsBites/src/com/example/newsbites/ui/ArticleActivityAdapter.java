package com.example.newsbites.ui;

import java.util.ArrayList;
import java.util.List;

import com.etsy.android.grid.util.DynamicHeightImageView;

import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.newsbites.*;

import com.example.newsbites.R;

public class ArticleActivityAdapter extends PagerAdapter {
	
	Context myContext;
	Activity activity;
	int resource;
	TopStories topStories;
	
	private LayoutInflater inflater;


	public ArticleActivityAdapter(Activity activity, int resource, TopStories objects, Context context) {
		super();

		this.activity = activity;
		this.resource = resource;
		this.topStories = objects;
		this.myContext = context;
		
		Log.v("TEST", "Variable set");
	}
	

    @Override
    public int getCount() {
    	
    	return 0;
      
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
      return view == ((ImageView) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
      
    	inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.article_page_view, container, false);
 
    	
    	final DealHolder holder;
		
		holder = new DealHolder();
		holder.image = (ImageView)viewLayout.findViewById(R.id.image);
		holder.title = (TextView)viewLayout.findViewById(R.id.title);
		
		Log.v("TEST", "Displaying " + topStories.getNewsBites().get(position));
		
		
		ATopStory data = topStories.getNewsBites().get(position);
		
		Log.v("TEST", "img_url " + data.getImgUrl());
		
		Picasso.with(myContext)
				.load(data.getImgUrl())
				.into(holder.image);
		
		Log.v("TEST", "image loaded");
		
		holder.title.setText(data.getTitle());
		
	    ((ViewPager) container).addView(viewLayout);
		
    	Log.v("TEST", "do you see the page now?");
	    
	    return data;
		
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
      ((ViewPager) container).removeView((ImageView) object);
    }

	
	static class DealHolder {
		ImageView image;
		TextView title;
	}

}
