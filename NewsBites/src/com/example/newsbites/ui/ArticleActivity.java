package com.example.newsbites.ui;

import com.example.newsbites.*;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.SystemClock;
import android.speech.tts.TextToSpeech;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;

import com.example.newsbites.R;
import com.example.newsbites.ui.*;
import com.example.newsbites.ui.ArticleActivityAdapter.DealHolder;
import com.example.newsbites.ui.PlayerService.PlayerBinder;
import com.squareup.picasso.Picasso;


import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import android.os.IBinder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.view.MenuItem;
import android.view.View;

public class ArticleActivity extends Activity implements MediaController.MediaPlayerControl,
TextToSpeech.OnInitListener, TextToSpeech.OnUtteranceCompletedListener{
	
	//OnPreparedListener

	Context myContext;
	TopStories TopStoriesArray;
	
	ViewPager viewPager;
    private ArticleActivityAdapter mAdapter;
    ImagePagerAdapter adapter;
    private LayoutInflater inflater;
    
    private int currentItem = -1;
    
    private MediaPlayer mediaPlayer;
	private MediaController mediaController;
	
	private TextToSpeech textToSpeech;	  
    final int MY_DATA_CHECK_CODE = 1;
	
    FileInputStream fis;
    FileDescriptor fd;
    
    private int itemArraySize = 0;
    
    
    ///////////////
    
    private PlayerService playerService;
    private Intent playIntent;
	private boolean playerBound=false;
    private int serviceStarted = 0;	
	private boolean paused=false, playbackPaused=false;
    private boolean ttsSet = false;
	
    private boolean calledOnce[];
    
    
    AutoPlayTimerTask myTimerTask = null;
    Timer ConnectionTimer = null;
    
    PlayerCheckTimerTask playerTimerTask = null;
    Timer playerTimer = null;
    
    boolean userInteractedOnce = false;
    
    /*
    private BiteController biteController;    
    private void setBiteController(){
    	  //set the controller up
    	biteController = new BiteController(this);
    	biteController.setPrevNextListeners(new View.OnClickListener() {
    		  @Override
    		  public void onClick(View v) {
    		    playNext();
    		  }
    		}, new View.OnClickListener() {
    		  @Override
    		  public void onClick(View v) {
    		    playPrev();
    		  }
    		});
    	biteController.setMediaPlayer(this);
    	biteController.setAnchorView(findViewById(R.id.main_audio_view));
    	biteController.setEnabled(true);
    	
    	biteController.show(0);
    	
    }
    */
    
    private boolean configChange = false;
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	if (keyCode == KeyEvent.KEYCODE_BACK) {
    		
    		Log.v("TEST", "******* back pressed *******");

    	}

    	return super.onKeyDown(keyCode, event);
 }
    
    //@Override
    public void onBackPressed() {
        // This will be called either automatically for you on 2.0
        // or later, or by the code above on earlier versions of the
        // platform.
    	
    	super.onBackPressed();
    	
    	Log.v("TEST", "******* back pressed *******");    	
    	System.exit(0);
    	
    	Intent intent = new Intent(myContext, CategoryActivity.class);				
		startActivity(intent);
    	
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        Log.v("TEST", "------ config change ------");
        
        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
        }
        
        configChange = true;
        
    }
    
    @Override
    protected void onPause()
    {
    	super.onPause();
    	
    	Log.v("TEST", "------ Activity paused ------");
    }
    
    @Override
    protected void onResume()
    {
    	super.onResume();
    	
    	Log.v("TEST", "------ Activity resumed ------");
    }
    
    
    
	@Override
	protected void onStart()
	{
		super.onStart();
		
		Log.v("TEST", "------ Article activity start ------");
		
	    if(playIntent==null){
		    
	    	playIntent = new Intent(this, PlayerService.class);
		    this.bindService(playIntent, audioConnection, Context.BIND_AUTO_CREATE);
		    this.startService(playIntent);
		    Log.v("TEST", "Starting service");
			    
		}
	    else
	    {
	    	Log.v("TEST", "playIntent not null");
	    }
	    
	    //Log.v("TEST", "playerService isPlaying" + playerService.isPlaying());
	        
	        
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);	
		
		Log.v("TEST", "Called on new intent");
		
	}
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.article_view);        
        
        //*
        myContext = getApplicationContext();        
        Intent i = getIntent();      
        
       
        TopStoriesArray = (TopStories) i.getSerializableExtra("topStories");
        itemArraySize = TopStoriesArray.getNewsBites().size();
       	
        Log.v("TEST", "Item array size: " + itemArraySize);
        
        setTitle("Top Story"); 
        
                
        //*/
       
        ///////////////////////////////////
        
        Log.v("TEST", "Article activity create");        
        calledOnce = new boolean[itemArraySize];
        resetCalledOnce();
        
        ////////////
        ////////////
        /*
        mediaPlayer = new MediaPlayer();        
	    mediaPlayer.setOnPreparedListener(this);
	    try
	    {
	    	mediaPlayer.setDataSource(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "NewsBite" + "/" + "temp.mp3");
	    }catch(IOException ie)
	    {
	    	
	    }
	    try {
			mediaPlayer.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			
			Log.v("TEST", "Illegal state");
			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    Log.v("TEST", "Preparing player");
	    
	    */
	    /////////////
	    /////////////
	    
	    mediaController = new MediaController(this){
	    	@Override
	    	public void hide()
	    	{
	    		super.show();
	    	}
	    	
	    };
        
    	// Fire off an intent to check if a TTS engine is installed
        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);
		//*/
	        
	    ///////////////////////////////////
	    
        /*
        viewPager = (ViewPager) findViewById(R.id.view_pager);        
        mAdapter = new ArticleActivityAdapter(this, R.layout.article_page_view, TopStoriesArray, myContext);
        viewPager.setAdapter(mAdapter);
        viewPager.setCurrentItem(0);
        */        
        
	    //last change
        //ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        //ImagePagerAdapter adapter = new ImagePagerAdapter();
        //viewPager.setAdapter(adapter);
        
        /*
        LayoutInflater inflater = getLayoutInflater();
        View rowView= inflater.inflate(R.layout.list_single, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        txtTitle.setText("Temp");
        imageView.setImageResource(R.drawable.chiang_mai);
        */
        
        /*
        final boolean customTitleSupported = requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
        if (customTitleSupported) {
            getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.titleBar);
        }
        */
        
        //WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
        //WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
        //WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON
        
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        
    }
    
    
    //-----------------------------------------------------------------------
    
    //connect to the service
  	private ServiceConnection audioConnection = new ServiceConnection(){
  	 
  	  @Override
  	  public void onServiceConnected(ComponentName name, IBinder service) {
  	    
  		PlayerBinder binder = (PlayerBinder)service;
  	    //get service
  	    playerService = binder.getService();
  	    //pass list
  	    playerService.setTopStories(TopStoriesArray);
  	    playerService.setContext(getApplicationContext());
  	    
  	    playerBound = true;
  	    
  	    Log.v("TEST", "service connected, ready to play bites");
  	    
  	    //start timer
		//myTimerTask = new AutoPlayTimerTask();
	    //ConnectionTimer = new Timer();  
	    //ConnectionTimer.schedule(myTimerTask, 5*1000);
	    
	    //playerTimerTask = new PlayerCheckTimerTask();
	    //playerTimer = new Timer();  
	    //playerTimer.schedule(playerTimerTask, 5*1000, 5*1000);
	    
	    
	    
	    Log.v("TEST", "Timer set");
  	    
  	  }
  	 
  	  @Override
  	  public void onServiceDisconnected(ComponentName name) {
  	    playerBound = false;
  	    
  	  Log.v("TEST", "service disconnected");
  	    
  	  }
  	};
  	
    
    //-----------------------------------------------------------------------
	
  	public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
          if (requestCode == MY_DATA_CHECK_CODE)
          {
              if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS)
              {
                  // success, create the TTS instance
            	  textToSpeech = new TextToSpeech(this, this);                
                  Toast.makeText( ArticleActivity.this, "TTS available", Toast.LENGTH_SHORT ).show();              
                  
              }
              else
              {
              	  Toast.makeText( ArticleActivity.this, "TTS not available", Toast.LENGTH_SHORT ).show();
              	
                  // missing data, install it
                  Intent installIntent = new Intent();
                  installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                  startActivity(installIntent);
              }
          }
    }
    
  	public void onInit(int status) {

        if (status == TextToSpeech.SUCCESS) {

            int result = textToSpeech.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
            	Log.e("TTS", "This Language is not supported");
            } else {
            
            	
            	if(playerBound == true)
            	{
            		if(ttsSet == false)
            		{
            			playerService.setTextToSpeech(textToSpeech);
            			ttsSet = true;
            			Log.v("TEST", "TextToSpeech set");
            		}	
            	}	
            	
            	viewPager = (ViewPager) findViewById(R.id.view_pager);
                adapter = new ImagePagerAdapter();
                viewPager.setAdapter(adapter);
                
                mediaController.setMediaPlayer(this);
        		mediaController.setAnchorView(findViewById(R.id.main_audio_view));
        		mediaController.setEnabled(true);
        		mediaController.show(0);
        		
        		
        		
        		//setBiteController();
        		
        		
                
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        }

	}
  	
  	public void resetCalledOnce()
    {
    	for(int i=0; i<itemArraySize; i++)
    	{
    		calledOnce[i] = false;
    	}
    }
    
    public void setCalledOnce(int position)
    {
    	calledOnce[position] = true;
    }
    
    private boolean autoCalled = false; 
    
    
    public void onUtteranceCompleted(String uttId) {
	    
	}
    @Override
	 public void onDestroy()
	 {
	        
		 	// Don't forget to shutdown!
	        
		 	if (textToSpeech != null)
	        {
	            textToSpeech.stop();
	            textToSpeech.shutdown();
	        }
		 	
		 	stopService(playIntent);
		 	playerService=null;
		 	
	        super.onDestroy();
	 }


    
    private class ImagePagerAdapter extends PagerAdapter {
    	
    	ListView list;
    	
    	Integer[] imageId = {
    		      R.drawable.facebook,
    		      R.drawable.twitter,
    		      R.drawable.linked,
    		      R.drawable.gplus,
    		      R.drawable.comments,
    		  };
    	
    	Integer[] countArray = { 0, 0, 0, 0, 0 };

        @Override
        public int getCount() {
          //return mImages.length;
        	return itemArraySize;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
          //return view == ((ImageView) object);
        	return view == ((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
        	
        	//return null;
        	
        	LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        	View layout = inflater.inflate(R.layout.article_page_view, null);
        	ImageView imageView=(ImageView) layout.findViewById(R.id.pageImage);             
        	
        	//1
        	TextView textView=(TextView) layout.findViewById(R.id.pageTitle);
        	
        	/*
        	TextView textFacebook = (TextView) layout.findViewById(R.id.textFacebook);
        	TextView textTwitter = (TextView) layout.findViewById(R.id.textTwitter);
        	TextView textLinkedIn = (TextView) layout.findViewById(R.id.textLinkedIn);
        	TextView textGplus = (TextView) layout.findViewById(R.id.textGplus);
        	TextView textComments = (TextView) layout.findViewById(R.id.textComments);
        	*/
        	
        	///*
        	if(position >= itemArraySize)
        	{
        		position = 0;
        	}
        	
        	Context context = ArticleActivity.this;
        	//ImageView imageView = new ImageView(context);          
            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        
            //Log.v("TEST", "Instantiate Pos: " + position);
            
            String newsTitle = TopStoriesArray.getNewsBites().get(position).getTitle();
            String imageUrl = TopStoriesArray.getNewsBites().get(position).getImgUrl();
            String newsSummary = TopStoriesArray.getNewsBites().get(position).getSummary();
            
            //Log.v("TEST", imageUrl);
            //Log.v("TEST", newsSummary);
            
            /*
            Bitmap bm = getResizedBitmap(getBitmapFromURL(imageUrl), 2500, 2500);
            imageView.setImageBitmap(bm);
            */
            
            //Picasso.with(context).load(imageUrl).resize(2200,3470).centerCrop().into(imageView);
            Picasso.with(context).load(imageUrl).fit().into(imageView);
            
            //TextView  textView = new TextView(context);
            
            
            //2
            textView.setText(newsTitle);
          
            
            
            /*
            if(textFacebook != null)
            {
            textFacebook.setText(String.valueOf(TopStoriesArray.getNewsBites().get(position).getFbCnt()));
            textTwitter.setText(String.valueOf(TopStoriesArray.getNewsBites().get(position).getTwCnt()));
            textLinkedIn.setText(String.valueOf(TopStoriesArray.getNewsBites().get(position).getFbCnt()));
            textGplus.setText(String.valueOf(TopStoriesArray.getNewsBites().get(position).getFbCnt()));
            textComments.setText(String.valueOf(TopStoriesArray.getNewsBites().get(position).getFbCnt()));
            }
            */
            
            ActionBar actionBar = getActionBar();
            actionBar.setSubtitle("By " + TopStoriesArray.getNewsBites().get(position).getAuthors() + " @" +
            TopStoriesArray.getNewsBites().get(position).getSource());
            actionBar.setTitle(newsTitle); 
            
            
            ((ViewPager) container).addView(layout, 0);
               
            //((ViewPager) container).addView(imageView, 0);
            //((ViewPager) container).addView(textView, 1);
            
            
            //return imageView;
            return layout;
            //*/
        }
        
        public Bitmap getBitmapFromURL(String src) {
            try {
                java.net.URL url = new java.net.URL(src);
                HttpURLConnection connection = (HttpURLConnection) url
                        .openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        
        public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
            int width = bm.getWidth();
            int height = bm.getHeight();
            float scaleWidth = ((float) newWidth) / width;
            float scaleHeight = ((float) newHeight) / height;
            // CREATE A MATRIX FOR THE MANIPULATION
            Matrix matrix = new Matrix();
            // RESIZE THE BIT MAP
            matrix.postScale(scaleWidth, scaleHeight);

            // "RECREATE" THE NEW BITMAP
            Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height,
                    matrix, false);

            return resizedBitmap;
        }
        
        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object)
        {	
        	
          if(position >= itemArraySize)
          {
          		position = 0;
          }
          
          if(calledOnce[position] == false)
          {
        	  resetCalledOnce();
        	  calledOnce[position] = true;
          }
          else
          {
        	  return ;
          }
          
          currentItem = position;
          
          LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      	  View layout = inflater.inflate(R.layout.article_page_view, null);
      	  ImageView imageView=(ImageView) layout.findViewById(R.id.pageImage);             
      	  
      	  //1
      	  TextView textView=(TextView) layout.findViewById(R.id.pageTitle);
      	  
      	  /*
      	  TextView textFacebook = (TextView) layout.findViewById(R.id.textFacebook);
      	  TextView textTwitter = (TextView) layout.findViewById(R.id.textTwitter);
      	  TextView textLinkedIn = (TextView) layout.findViewById(R.id.textLinkedIn);
      	  TextView textGplus = (TextView) layout.findViewById(R.id.textGplus);
      	  TextView textComments = (TextView) layout.findViewById(R.id.textComments);
      	  */
      	  
      		
          Log.v("TEST", "position " + position + "itemArraySize " + itemArraySize);	
          
        	
          if(ttsSet == false)
          {
        		playerService.setTextToSpeech(textToSpeech);
        		ttsSet = true;
    			Log.v("TEST", "TextToSpeech set");
          }
        	
          Context context = ArticleActivity.this;        	
          //ImageView imageView = new ImageView(context);          
          imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);          
          
          //Log.v("TEST", "Set primary Pos: " + position);
          
          String newsTitle = TopStoriesArray.getNewsBites().get(position).getTitle();
          String imageUrl = TopStoriesArray.getNewsBites().get(position).getImgUrl();
          String newsSummary = TopStoriesArray.getNewsBites().get(position).getSummary();
          
          //Log.v("TEST", imageUrl);
          //Log.v("TEST", newsSummary);
          
          /*
          Bitmap bm = getResizedBitmap(getBitmapFromURL(imageUrl), 2500, 2500);
          imageView.setImageBitmap(bm);
          */
          
          //Picasso.with(context).load(imageUrl).resize(2200,3470).centerCrop().into(imageView);
          Picasso.with(context).load(imageUrl).fit().into(imageView);
          
          //TextView  textView = new TextView(context);
          
          
          //2
          textView.setText(newsTitle);  
          
          
          
          
          /*
          if(textFacebook != null)
          {
          textFacebook.setText(String.valueOf(TopStoriesArray.getNewsBites().get(position).getFbCnt()));
          textTwitter.setText(String.valueOf(TopStoriesArray.getNewsBites().get(position).getTwCnt()));
          textLinkedIn.setText(String.valueOf(TopStoriesArray.getNewsBites().get(position).getFbCnt()));
          textGplus.setText(String.valueOf(TopStoriesArray.getNewsBites().get(position).getFbCnt()));
          textComments.setText(String.valueOf(TopStoriesArray.getNewsBites().get(position).getFbCnt()));
          }
          */
          
          
          ActionBar actionBar = getActionBar();
          actionBar.setSubtitle("By " + TopStoriesArray.getNewsBites().get(position).getAuthors() + " @" +
                  TopStoriesArray.getNewsBites().get(position).getSource());
          actionBar.setTitle(newsTitle); 
          
          
          
          /*
          inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      	  layout = inflater.inflate(R.layout.title_bar, null);
          TextView activityTitle = (TextView) findViewById(layout.findViewById(R.id.TitleTitleBar));
      	  ImageView activityImage = (ImageView) findViewById(layout.findViewById(R.id.ImageTitleBar));        
      	  Picasso.with(context)
		  .load(imageUrl)
		  .into(activityImage);      
      	  activityTitle.setText(newsTitle);
      	  */
          
          //Log.v("TEST", "Loaded: " + imageUrl);          
          //imageView.setImageResource(mImages[position]);
          
          /*
          countArray[0] = TopStoriesArray.getNewsBites().get(position).getFbCnt();
          countArray[1] = TopStoriesArray.getNewsBites().get(position).getTwCnt();
          
          ArticleInfo adapter = new ArticleInfo(ArticleActivity.this, countArray, imageId);
          list=(ListView)findViewById(R.id.pageList);
          list.setAdapter(adapter);
          list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        	  @Override
        	  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        	                    
        	  }
          });
          */
          
          if(configChange == true)
          {
        	  configChange = false;
        	  return ;
          }
          
         if(playerService.isPlaying() == true || playerService.mpState == 2)
         { 
        	 Log.v("TEST", "is playing: " + playerService.isPlaying());
        	 Log.v("TEST", "state: " + playerService.mpState);
        	 playerService.setPosition(position);
        	 playerService.stopBite();
        	 playerService.loadBite();
        	 
        	 myTimerTask = new AutoPlayTimerTask();
 		     ConnectionTimer = new Timer();  
 		     ConnectionTimer.schedule(myTimerTask, 5*1000); 			
        	 
        	 
         }
         else
         {
        	 Log.v("TEST", "is playing: " + playerService.isPlaying());
        	 Log.v("TEST", "state: " + playerService.mpState);         
        	 playerService.setPosition(position);
        	 playerService.loadBite();
        	 
        	 if(autoCalled == false && userInteractedOnce == true)
        	 {
        		 myTimerTask = new AutoPlayTimerTask();
     		     ConnectionTimer = new Timer();  
     		     ConnectionTimer.schedule(myTimerTask, 5*1000); 			
            	 
        	 }
         }
          
         /////////////////
         /////////////////
          
          /*
          HashMap<String, String> myHashRender = new HashMap<String, String>();
          myHashRender.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, newsSummary);
         
          String ext_storage_state = Environment.getExternalStorageState();
          File mediaStorage = new File(Environment.getExternalStorageDirectory() + "/NewsBite");
          if (ext_storage_state.equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
                if (!mediaStorage.exists()) {
                    mediaStorage.mkdirs();
                }
          } 
          
          try
          {             
        	  File file = new File(mediaStorage, "temp.mp3");
        	  if(!file.exists())
        	  {
        		  file.createNewFile();
        	  }
        	  
          }catch(Exception e)
          {
        	  Log.v("TEST", "File exception");
          }
          
          
          String exStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "NewsBite";
          String tempFileName = "temp.mp3";          
          String tempDestFile = exStoragePath + "/" + tempFileName;
         
          try
          {
        	  fis = new FileInputStream(tempDestFile);
        	  fd = fis.getFD();
          }catch(FileNotFoundException e){
        	  
        	  Log.v("TEST", "File not found");
        	  
          }catch(IOException e){
        	  
        	  Log.v("TEST", "IOException");
          }         
          
          textToSpeech.synthesizeToFile(newsSummary, myHashRender, tempDestFile);
          */
          
          /////////////////
          /////////////////
          
          
           /*
			try{
				  //mediaPlayer.setDataSource(getApplicationContext(), trackUri);
				mediaPlayer.setDataSource(fd);
			}
			catch(Exception e){
				  Log.v("TEST", "Error setting data source", e);
			}
			*/
			
			//mediaPlayer.prepareAsync();
			
			/*
			try
			{
			mediaPlayer.prepare();
			}catch(IOException e)
			{
				
			}
			*/
			//mediaPlayer.start();
	        
	        //*/
	        ///////////////////////////////////
                             
          	//((ViewPager) container).addView(imageView, 0);         	
          	
	        //return imageView;
          
        }
        
        class DealHolder {
    		ImageView image;
    		TextView title;
    	}


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
          ((ViewPager) container).removeView((View) object);
        }
        
        
      }
    
      private boolean mediaPlayerReleased = false;
    
	  @Override
	  protected void onStop() {
		
	    super.onStop();
	    
	    Log.v("TEST", "------ Activity stopped ------");
	    
	    /*
	    try{
	    fis.close();
	    }catch(IOException e){	
	    }	    
	    */
	    
	    mediaController.hide();
	    
	    mediaPlayerReleased = true;
		
	    
	    /*
	    Log.v("TEST", "3");
	    if(mediaPlayer.isPlaying())
	    {
	    	mediaPlayer.stop();
	    }
	    else
	    {
	    	Log.v("TEST", "Player not playing");
	    }
	    Log.v("TEST", "4");
	    mediaPlayer.release();
	    */
	    
	  }


		@Override
		public void pause() {
			// TODO Auto-generated method stub
			 //mediaPlayer.pause();
			
			if(playerService.isPlaying())
				playerService.pauseBite();		

			Log.v("TEST", "Pause call returned");
			
			
			//mediaPlayer.pause();
		}

	/*  
	  
	@Override
	public boolean onTouchEvent(MotionEvent event) {
	    //the MediaController will hide after 3 seconds - tap the screen to make it appear again
		
		Log.v("TEST", "Screen touched");		  
	    //mediaController.show(0);
	    return false;
	}
	*/
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		
		//Uncomment to enable action
		//
		//MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.activity_main_actions, menu);
        //
        
        return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean canPause() {
		// TODO Auto-generated method stub
		
		//Log.v("TEST", "Can pause called");
		
		return true;
	}

	@Override
	public boolean canSeekBackward() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean canSeekForward() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public int getAudioSessionId() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getBufferPercentage() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getCurrentPosition() {
		// TODO Auto-generated method stub
		if(playerService !=null && playerBound==true) //&& playerService.isPlaying())
		    return playerService.getPlayerPosition();
		else 
			return 0;
	}

	@Override
	public int getDuration() {
		// TODO Auto-generated method stub
		if(playerService!=null && playerBound==true) //&& playerService.isPlaying())
		    return playerService.getPlayerDuration();
		else 
			return 0;
	}

	@Override
	public boolean isPlaying() {
		// TODO Auto-generated method stub
		//return mediaPlayer.isPlaying();
		
		
		Log.v("TEST", ">>>> isPlaying called");
		
		if(playerService.isBiteCompletedSet() == true)
		{
			
			Log.v("TEST", "Bite played completely, auto sliding");
			
			//autoslide
			int nextItem = currentItem + 1;
			if(nextItem >= adapter.getCount())
			{
				//nextItem = adapter.getCount();				
				return playerService.isPlaying();
			}
			viewPager.setCurrentItem(nextItem, true);
			
			
			//start timer
			myTimerTask = new AutoPlayTimerTask();
		    ConnectionTimer = new Timer();  
		    ConnectionTimer.schedule(myTimerTask, 5*1000);
			
		    autoCalled = true;
			
			//unsetBiteComplete
			playerService.unsetBiteCompleted();
		}
		
		if(playerService != null && playerBound == true)
		{
			
			//Log.v("TEST", "PlayerService is not null " + playerService.isPlaying());			
			return playerService.isPlaying();
			
		}
		else
		{
			//Log.v("TEST", "PlayerService is null");			
			return false;
		}
		
		
		/*
		if(mediaPlayerReleased == false)
		{
			return playerService.isPlaying();
		}
		else
		{
			return false;
		}
		//*/
	}	

	@Override
	public void seekTo(int pos) {
		// TODO Auto-generated method stub
		
		playerService.seek(pos);
		
	}
	
	
	boolean playerPrepared = false;
	@Override
	public void start() {
		// TODO Auto-generated method stub
		
		Log.v("TEST", "Start called");
		
		if(userInteractedOnce == false)
			userInteractedOnce = true;
		
		playerService.startBite();
		
		Log.v("TEST", "Start call returned");
		
		
		/*
		Log.v("TEST","Play pressed");
		if(mediaPlayer.isPlaying())
		{
			Log.v("TEST", "Playing");
			mediaPlayer.start();
		}
		else
		{
			Log.v("TEST", "Not Playing");
			if(playerPrepared == false)
			{
				//mediaPlayer.prepareAsync();
				playerPrepared = true;
			}
			else
			{
				mediaPlayer.start();
			}
		}
		*/
		
		
	}
	
	//@Override
	public void playNext()
	{
		int nextItem = currentItem + 1;
		if(nextItem >= adapter.getCount())
		{
			nextItem = adapter.getCount();
		}
		viewPager.setCurrentItem(nextItem, true);
		
	}
	
	//@Override
	public void playPrev()
	{
		int nextItem = currentItem - 1;
		if(nextItem < 0)
		{
			nextItem = 0;
		}
		viewPager.setCurrentItem(nextItem, true);
		
	}
	
	class AutoPlayTimerTask extends TimerTask {
		  public void run() {
			
			 Log.v("TEST", "Timer called"); 
			  
			if(playerService.isPlaying() == false)
			{
				//start();
				playerService.startBite();
			}	
			  
		  }
		}
		
    
	class PlayerCheckTimerTask extends TimerTask {
		  public void run() {
			
			 Log.v("TEST", "Player check Timer called");
			 
			 
			 if(playerService.isPlaying() == false)
			 {
				 if(playerService.isBiteCompletedSet() == true)
				 {
					
					Log.v("TEST", "*** Timer: Bite played completely, auto sliding ***");
					
					/*
					//autoslide
					int nextItem = currentItem + 1;
					if(nextItem >= adapter.getCount())
					{
						nextItem = 0;
					}
					viewPager.setCurrentItem(nextItem, true);
					
					/*
					//start timer
					myTimerTask = new AutoPlayTimerTask();
				    ConnectionTimer = new Timer();  
				    ConnectionTimer.schedule(myTimerTask, 5*1000);
					
				    autoCalled = true;
				    
					//unsetBiteComplete
					playerService.unsetBiteCompleted();
					
					*/
					
				 }
			 }	 
			  
		  }
		}
	
	
	/*
    @Override
	public void onPrepared(MediaPlayer mp) {
		// TODO Auto-generated method stub
		//start playback
		
		Log.v("TEST", "Player prepared");
		playerPrepared = true;
		
		
		//mp.start();
		  
		
		//mediaController.setMediaPlayer(this);
		//mediaController.setAnchorView(findViewById(R.id.main_audio_view));
		//mediaController.setEnabled(true);
		
	    
		mediaController.show(0);
	      
	}
    */
    
}
