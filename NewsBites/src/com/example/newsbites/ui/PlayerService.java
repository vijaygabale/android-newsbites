package com.example.newsbites.ui;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentUris;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.Environment;
import android.os.PowerManager;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;
import android.widget.RemoteViews;

import java.util.Random;

import com.example.newsbites.R;
import com.example.newsbites.TopStories;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;

@SuppressWarnings("deprecation")
public class PlayerService extends Service implements
MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener,
MediaPlayer.OnCompletionListener {
	
	//media player
	private MediaPlayer player;	
	private final IBinder playerBind = new PlayerBinder();	
	private int bitePosition = 0;
	
	
	private TextToSpeech textToSpeech;	  
    final int MY_DATA_CHECK_CODE = 1;
	FileInputStream fis;
    FileDescriptor fd;
	private TopStories TopStoriesArray;
	private static final int NOTIFY_ID=1;
	
	private String biteTitle = "Top Stories";
	private String biteTitleRemoveSpace = "";
	
	//0 = stopped
	//1 = started
	//2 = paused
	
	public int mpState = -1;
	
	
	public int playerStarted = 0;
	
	public boolean thisBiteCompleted = false;
	
	private Context myContext;
		
	public void setTextToSpeech(TextToSpeech tts)
	{
		textToSpeech = tts;
		
		textToSpeech.setOnUtteranceProgressListener(new UtteranceProgressListener() {

			@Override
			public void onDone(String arg0) {
				// TODO Auto-generated method stub
			//prepareMusicPlayer(arg0);
				
			}

			@Override
			public void onError(String arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStart(String utteranceId) {
				// TODO Auto-generated method stub
				
				Log.v("TEST", "Utterance progress started ... ");
				
				
								
			}
        });
		
	}
	
	public void setTopStories(TopStories objects)
	{
		TopStoriesArray = objects;			
		biteTitle = TopStoriesArray.getTitle();
		
		Log.v("TEST", "bite title " + biteTitle);
		biteTitleRemoveSpace = biteTitle.replace(" ", "");
		
		String ext_storage_state = Environment.getExternalStorageState();
        File mediaStorage = new File(Environment.getExternalStorageDirectory() + "/NewsBite" + "/" + biteTitleRemoveSpace);
        if (ext_storage_state.equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
              if (!mediaStorage.exists()) {
                  mediaStorage.mkdirs();
              }
              else
              {
            	  if (mediaStorage.isDirectory())
            	        for (File child : mediaStorage.listFiles())
            	        	child.delete();
              }
        } 
        
		
	}
	
	public void setContext(Context iContext)
	{
		myContext = iContext;
	}
	
	public void setPosition(int position)
	{
		bitePosition = position;
	}
	
	private boolean playerPrepared = false;
	
	public void loadBite()
	{	
		
		//playerPrepared = false;		
		boolean fileExists = false;		
		biteTitle = TopStoriesArray.getNewsBites().get(bitePosition).getTitle();
		
		String newsTitle = TopStoriesArray.getNewsBites().get(bitePosition).getTitle();
		String newsSummary = TopStoriesArray.getNewsBites().get(bitePosition).getSummary();		
		
        String ext_storage_state = Environment.getExternalStorageState();
        File mediaStorage = new File(Environment.getExternalStorageDirectory() + "/NewsBite");  //+ "/" + biteTitleRemoveSpace);
        if (ext_storage_state.equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
              if (!mediaStorage.exists()) {
                  mediaStorage.mkdirs();
              }
        } 
        
        String fileName = bitePosition + "audio.mp3"; //1.mp3
        
        try
        {	
      	  //File file = new File(mediaStorage, "1.mp3");
          File file = new File(mediaStorage, fileName);	
      	  if(!file.exists())
      	  {
      		  file.createNewFile();
      		  file.setReadable(true);
      		  file.setWritable(true);
      	  }
      	  else
      	  {
      		  fileExists = true;
      	  }
      	  
        }catch(Exception e)
        {
      	  Log.v("TEST", "File exception");
        }
        
        
        String exStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "NewsBite"; //+ "/" + biteTitleRemoveSpace;                 
        String destFile = exStoragePath + "/" + fileName;       
        
        /*
        try
        {
      	  fis = new FileInputStream(destFile);
      	  fd = fis.getFD();
        }catch(FileNotFoundException e){
      	  
      	  Log.v("TEST", "File not found");
      	  
        }catch(IOException e){
      	  
      	  Log.v("TEST", "IOException: loadBite, loadingstream");
        }

        */
        
        player.reset();
        //player.release();
        playerPrepared = false;       
        prepareMusicPlayer(destFile);
        
        HashMap<String, String> myHashRender = new HashMap<String, String>();
        myHashRender.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, destFile);
       
        
        //if(fileExists == false)
        textToSpeech.synthesizeToFile(newsSummary, myHashRender, destFile);
        
        //t1 = System.currentTimeMillis();
        
        //Log.v("TEST", "media player: change track");
        
        
        //initMusicPlayer();
        
        
        /*
        try
	    {
	    	player.setDataSource(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "NewsBite/" + "1.mp3");
	    	
	    	t3 = System.currentTimeMillis();
	    	
	    }catch(IOException ie)
	    {
	    	Log.v("TEST", "IOException: loadBite, setdatasource");
	    }
	    try {
			player.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			
			Log.v("TEST", "Illegal state");
			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
			Log.v("TEST", "IOException: loadBite, prepare ");
			
			e.printStackTrace();
		}
	    
	    Log.v("TEST", "Preparing player...");
        
	    */
	    
        //Log.v("TEST", "resetting player");
        //player.reset();
        //prepareMusicPlayer();
        
	}
	
	public void onCreate(){
		//create the service
		super.onCreate();
		//create player
		
		bitePosition = 0;		
		initMusicPlayer();		
		Log.v("TEST", "Media Player Initialized");
		
	}
	
	public void prepareMusicPlayer(String filePath)
	{
		
		//filePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "NewsBite/Technology/" + "0audio.mp3";
		
		try
	    {
	    	player.setDataSource(filePath);
	    }catch(IOException ie)
	    {
	    	Log.v("TEST", "IOException: in PMP");
	    }
	    try {
			player.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			
			Log.v("TEST", "Illegal state: in PMP");
			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			
			Log.v("TEST", "IOE: in PMP, prepare");
			
			e.printStackTrace();
		}
	    
	    Log.v("TEST", "Preparing player...");
	}
	
	public void initMusicPlayer()
	{
		//set player properties
		player = new MediaPlayer();
		player.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
		player.setAudioStreamType(AudioManager.STREAM_MUSIC);
		player.setOnPreparedListener(this);
		player.setOnCompletionListener(this);
		player.setOnErrorListener(this);
		
		/*
		try
	    {
	    	player.setDataSource(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "NewsBite" + "/" + "1.mp3");
	    }catch(IOException ie)
	    {
	    	
	    }
	    try {
			player.prepare();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			
			Log.v("TEST", "Illegal state");
			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    Log.v("TEST", "Preparing player...");
		*/
		
	}
	

	public class PlayerBinder extends Binder {
		  public PlayerService getService() {
		    return PlayerService.this;
		  }
	}
	

	@Override
	public boolean onUnbind(Intent intent)
	{
		 player.stop();
		 player.release();
		 return false;
	}
	
	@Override
	public void onDestroy() {
	  stopForeground(true);
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return playerBind;
		
	}

	public boolean isBiteCompletedSet()
	{
		return thisBiteCompleted;
	}
	
	public void unsetBiteCompleted()
	{
		thisBiteCompleted = false;
	}
	
	private int counterI = 0;
	
	@Override
	public void onCompletion(MediaPlayer mp) {
		// TODO Auto-generated method stub
		
		//Log.v("TEST", "On completion");		
		//player.stop();
		//mpState = 0;
		
		/*
		if(playerStarted == 0)
		{
			playerStarted = 1;
		}
		else
		{
		*/	
			
		Log.v("TEST", "Newsbite completeted " + counterI);
		counterI = counterI  + 1;
		Log.v("TEST", "counterI " + counterI);
		
		//if(counterI == 1)
			//playerStarted = 1;
		
		//if(playerStarted == 0)
		//{
		//	playerStarted = 1;
		//}
		//else
		//{
			thisBiteCompleted = true;
			//playerStarted = 0;
		//}	
			
		//}	
		
		
		
	}

	@Override
	public boolean onError(MediaPlayer mp, int what, int extra) {
		// TODO Auto-generated method stub
		player.reset();
		return false;
	}
	
	private boolean notificationSet = false;
	
	public void setNotifyIntent()
	{
		
		/*
		Context myContext = getApplicationContext();		
		String ns = myContext.NOTIFICATION_SERVICE;
		NotificationManager mNotificationManager = (NotificationManager) myContext.getSystemService(ns);
		*/
		
		Intent notifyIntent = new Intent(getApplicationContext(), NotificationActivity.class);		
		
		/*
		notifyIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);		
		notifyIntent.setAction(Intent.ACTION_MAIN);
		notifyIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		notifyIntent.setAction("android.intent.action.MAIN");
		notifyIntent.addCategory("android.intent.category.LAUNCHER");    
		*/
		
		//notIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		//PendingIntent pendInt = PendingIntent.getActivity(this, 0,  notIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		 
		PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, notifyIntent, PendingIntent.FLAG_ONE_SHOT); //PendingIntent.FLAG_UPDATE_CURRENT);
		
		Notification.Builder builder = new Notification.Builder(this);
		 
		builder.setContentIntent(pendingIntent)
		  .setSmallIcon(R.drawable.play)
		  .setTicker("NewsBite")
		  .setOngoing(true)
		  .setContentTitle("Now Playing")
		  .setContentText(biteTitle);
		
		Notification myNotification = builder.build();		 
		startForeground(NOTIFY_ID, myNotification);		
		notificationSet = true;
		
		Log.v("TEST", "notification set");		

	}

	
	@Override
	public void onPrepared(MediaPlayer mp) {
		// TODO Auto-generated method stub		
		//you can start playback here		
		//if(notificationSet == false)
		//{
		
		setNotifyIntent();
		
		//setNotification();
		
		//}
		
		playerPrepared = true;		
		Log.v("TEST", "player prepared (in service)");
		
		if(TopStoriesArray != null)
		{
			if(textToSpeech != null)
			{
				//loadBite();				
				//playout				
				//player.start();				
			}
			else
			{
				Log.v("TEST", "Text to speech is null");
			}
		}
		
		/*
		if(playerNotPreparedStartPressed == true)
		{
			Log.v("TEST", "Player Not Prepared, start pressed, error...");
		}
		*/
		
		///*
		if(playerNotPreparedStartPressed == true)
		{
			player.start();
			mpState = 1;
			playerNotPreparedStartPressed = false;			
			Log.v("TEST", "Player Not Prepared, start pressed, starting...");
		}
		//*/
		
	}

	public void resetPlayer()
	{
		
		Log.v("TEST", "reset: state " + mpState);
		
		if(player.isPlaying())
		{
			player.stop();
			mpState = 0;
		}	
		
		player.reset();
		
	}
	
	private boolean playerNotPreparedStartPressed = false;
	
	public void startBite()
	{
		
		Log.v("TEST", "start: prev State " + mpState);
		
		if(playerPrepared == false)
		{
			
			playerNotPreparedStartPressed = true;
			
			Log.v("TEST", "Player Not Prepared, start pressed");
		
			/*
			try {
				player.prepare();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				
				Log.v("TEST", "Illegal state");
				
				resetPlayer();
				initMusicPlayer();
				
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    Log.v("TEST", "Preparing player...");
			
		    */
		    
			return ;
			
		}
		
		player.start();
		mpState = 1;
		
		/*
		if(mpState != 1)
		{
			player.start();
			mpState = 1;
		}
		else
		{
			player.stop();
			player.start();
			mpState = 1;
		}
		*/
		
		Log.v("TEST", "new State " + mpState);
		
	}
	
	public void pauseBite()
	{
		Log.v("TEST", "pause: prev State " + mpState);
		if(mpState != 2)
		{
			playerStarted = 1;
			player.pause();
			mpState = 2;
		}	
		Log.v("TEST", "new State " + mpState);
	}
	
	public void stopBite()
	{
		Log.v("TEST", "stop: prev State " + mpState);
		if(player.isPlaying() == true || mpState == 2)
		{
			Log.v("TEST", "Stopping player");
			player.stop();
			mpState = 0;

			/*
			playerPrepared = false;
			Log.v("TEST", "new State " + mpState);
			
			player.reset();			
			String fileName = bitePosition + "audio.mp3"; //1.mp3
	        
	        try
	        {
	        
	          String ext_storage_state = Environment.getExternalStorageState();
	          File mediaStorage = new File(Environment.getExternalStorageDirectory() + "/NewsBite" + "/" + biteTitleRemoveSpace);
	          if (ext_storage_state.equalsIgnoreCase(Environment.MEDIA_MOUNTED)) 
	          {
	                  if (!mediaStorage.exists()) 
	                  {
	                      mediaStorage.mkdirs();
	                  }
	          } 	
	        	
	          File file = new File(mediaStorage, fileName);	        	
	      	  if(!file.exists())
	      	  {
	      		  file.createNewFile();
	      	  }
	      	  
	        }catch(Exception e) {
	      	  Log.v("TEST", "File exception");
	        }
			
	        String exStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "NewsBite" + "/" + biteTitleRemoveSpace;                 
	        String destFile = exStoragePath + "/" + fileName;
	        
			try {
		    	//player.setDataSource(Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + "NewsBite" + "/" + "1.mp3");
				player.setDataSource(destFile);
				
		    }catch(IOException ie) {
		    	Log.v("TEST", "IOException: stopBite, settingdatasource");		    	
		    	ie.printStackTrace();	    	
		    }
			
			
			try {
				player.prepare();
				Log.v("TEST", "Preparing player...");
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				Log.v("TEST", "Illegal state, initiating music player");
				initMusicPlayer();
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.v("TEST", "IOException: stopBite, prepare");
				e.printStackTrace();
			}
			*/
		    
		}	
	}
	
	public int getPlayerPosition(){
		  return player.getCurrentPosition();
	}
		 
	public int getPlayerDuration(){
		  return player.getDuration();
	}
		 
	public boolean isPlaying(){
		  return player.isPlaying();
	}
		 
	public void seek(int position){
		  player.seekTo(position);
	}
	
	//skip to next
	public void playNext(){
		  
	}
		
	public void playPrev(){
			  
	}
	
	
	
	public void setNotification(){
		
		//RemoteViews notificationView = new RemoteViews(getPackageName(), R.layout.notification_bar_controller);
		RemoteViews notificationView = new RemoteViews(getPackageName(), R.layout.custom_notification);
		
		/*
	    Intent playIntent = new Intent("com.example.app.ACTION_PLAY");
	    PendingIntent pendingPlayIntent = PendingIntent.getBroadcast(myContext, 0, playIntent, 0);
	    notificationView.setOnClickPendingIntent(R.id.btn_play, pendingPlayIntent);
	   
	    Intent nextIntent = new Intent("com.example.app.ACTION_NEXT");
	    PendingIntent pendingNextIntent = PendingIntent.getBroadcast(myContext, 0, nextIntent, 0);
	    notificationView.setOnClickPendingIntent(R.id.btn_next, pendingNextIntent);
	   
	    Intent prevIntent = new Intent("com.example.app.ACTION_PREV");
	    PendingIntent pendingPrevIntent = PendingIntent.getBroadcast(myContext, 0, prevIntent, 0);
	    notificationView.setOnClickPendingIntent(R.id.btn_prev, pendingPrevIntent);
	   
	   
	    // instance of custom broadcast receiver
	    AudioPlayerBroadcastReceiver broadcastReceiver = new AudioPlayerBroadcastReceiver();
	    
	    IntentFilter intentFilterPlay = new IntentFilter();
	    intentFilterPlay.addCategory(Intent.CATEGORY_DEFAULT);
	    // set the custom action
	    intentFilterPlay.addAction("com.example.app.ACTION_PLAY");
	    
	    IntentFilter intentFilterNext = new IntentFilter();
	    intentFilterNext.addCategory(Intent.CATEGORY_DEFAULT);
	    // set the custom action
	    intentFilterNext.addAction("com.example.app.ACTION_NEXT");
	    
	    IntentFilter intentFilterPrev = new IntentFilter();
	    intentFilterPrev.addCategory(Intent.CATEGORY_DEFAULT);
	    // set the custom action
	    intentFilterPrev.addAction("com.example.app.ACTION_PREV");
	    
	    // register the receiver
	    registerReceiver(broadcastReceiver, intentFilterPlay);
	    registerReceiver(broadcastReceiver, intentFilterNext);
	    registerReceiver(broadcastReceiver, intentFilterPrev);
	    
	    */
	    
	    ///*
		String ns = Context.NOTIFICATION_SERVICE;
	    NotificationManager notificationManager = (NotificationManager) getSystemService(ns);

	    int icon = R.drawable.play;
	    long when = System.currentTimeMillis();
	    Notification notification = new Notification(icon, "Notification", when);
	    
	    
	    notificationView.setImageViewResource(R.id.notification_image, R.drawable.play);
	    notificationView.setTextViewText(R.id.notification_title, "Now Playing");
	    notificationView.setTextViewText(R.id.notification_text, biteTitle);
	    notification.contentView = notificationView;
	    
	    
	    //Notification.Builder builder = new Notification.Builder(this);
	    //Notification notification = builder.getNotification();


	    //the intent that is started when the notification is clicked (works)
	    Intent notificationIntent = new Intent(this, NotificationActivity.class);
	    PendingIntent pendingNotificationIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

	    
	    notification.contentIntent = pendingNotificationIntent;
	    notification.flags |= Notification.FLAG_NO_CLEAR; //Do not clear the notification
	    notification.defaults |= Notification.DEFAULT_LIGHTS; // LED
	    notification.defaults |= Notification.DEFAULT_VIBRATE; //Vibration
	    notification.defaults |= Notification.DEFAULT_SOUND; // Sound
	    
	    /*
	    notification.tickerText=biteTitle;
	    notification.icon=R.drawable.play;
	    notification.contentView = notificationView;
	    notification.contentIntent = pendingNotificationIntent;
	    notification.flags |= Notification.FLAG_NO_CLEAR;
	    */
	    
	    notificationManager.notify(NOTIFY_ID, notification);	    
		//*/


	    
	    /*
	    Intent notifyIntent = new Intent(getApplicationContext(), NotificationActivity.class);		
		PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, notifyIntent, PendingIntent.FLAG_ONE_SHOT); //PendingIntent.FLAG_UPDATE_CURRENT);
		Notification.Builder builder = new Notification.Builder(this);
		 
		
		builder.setContentIntent(pendingIntent)
		  .setSmallIcon(R.drawable.play)
		  .setTicker("NewsBite")
		  .setOngoing(true)
		  .setContentTitle("Now Playing")
		  .setContentText(biteTitle);
		  //.setContent(notificationView);
		
		Notification myNotification = builder.build();
		myNotification.contentView = notificationView;
		startForeground(NOTIFY_ID, myNotification);		
		*/
		
		notificationSet = true;
		Log.v("TEST", "notification set");	
	    
	    
	}
	
	public class AudioPlayerBroadcastReceiver extends BroadcastReceiver{
		@Override
		public void onReceive(Context context, Intent intent) {

		    String action = intent.getAction();

		    if(action.equalsIgnoreCase("com.example.app.ACTION_PLAY")){
		        // do your stuff to play action;
		    	
		    	Log.v("TEST", "play called");
		    	
		    }
		    
		    if(action.equalsIgnoreCase("com.example.app.ACTION_NEXT")){
		        // do your stuff to play action;
		    	
		    	Log.v("TEST", "next called");
		    	
		    }
		    
		    if(action.equalsIgnoreCase("com.example.app.ACTION_PREV")){
		        // do your stuff to play action;
		    	
		    	Log.v("TEST", "prev called");
		    	
		    }
		    
		}
	}


}
