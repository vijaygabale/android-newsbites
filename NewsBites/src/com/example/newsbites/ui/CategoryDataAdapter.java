package com.example.newsbites.ui;
import com.example.newsbites.*;

import java.util.List;

import com.etsy.android.grid.util.DynamicHeightImageView;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;


public class CategoryDataAdapter extends ArrayAdapter<ANewsCategory> {
	
	Activity activity;
	int resource;
	//List<Data> datas;
	List<ANewsCategory> newsCategories;

	//public CategoryDataAdapter(Activity activity, int resource, List<Data> objects) {
	public CategoryDataAdapter(Activity activity, int resource, List<ANewsCategory> objects) {
		super(activity, resource, objects);

		this.activity = activity;
		this.resource = resource;
		//this.datas = objects;
		this.newsCategories = objects;
		
		Log.v("TEST","Inside constructor " + objects.get(0).getTitle());
		
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		final DealHolder holder;
		
		if (row == null) {
			LayoutInflater inflater = activity.getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			
			holder = new DealHolder();
			holder.image = (DynamicHeightImageView)row.findViewById(R.id.image);
			holder.title = (TextView)row.findViewById(R.id.title);
			holder.description = (TextView)row.findViewById(R.id.description);
			
			row.setTag(holder);
		}
		else {
			holder = (DealHolder) row.getTag();
		}
		
		final ANewsCategory data = newsCategories.get(position);
		
		String imageLink = "http://www.beck-technology.com/images/bt_image00_home.jpg";
		if(data.getName().contains("technology"))
		{
			imageLink = "http://www.beck-technology.com/images/bt_image00_home.jpg";
		}
		else
		{
			imageLink = "http://downtrend.com/wp-content/uploads/2014/05/TopStory2_10230.jpg";
		}
		
		Picasso.with(this.getContext())
				.load(imageLink)
				.into(holder.image);
		
		holder.image.setHeightRatio(1.0);
		holder.title.setText(data.getTitle());
		holder.description.setText(data.getName());
		
		return row;
	}
	
	static class DealHolder {
		DynamicHeightImageView image;
		TextView title;
		TextView description;
	}

}
