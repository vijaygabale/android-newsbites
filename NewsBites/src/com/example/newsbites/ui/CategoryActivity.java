package com.example.newsbites.ui;

import com.example.newsbites.*;
import com.example.newsbites.MainActivity.NewsCategoriesSpiceRequestListener;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.util.ArrayList;

public class CategoryActivity extends BaseSpiceActivity implements AbsListView.OnItemClickListener {


    private StaggeredGridView mGridView;
    private CategoryDataAdapter mAdapter;
    
    Context myContext;
    NewsCategories newsCategories;
    
	private SpiceRequestNewsCategories spiceRequestNewsCategories;

    private SpiceRequestTopStories spiceRequestTopStories;
    private SpiceRequestTechnology spiceRequestTechnology;
    
    ///*
    @Override
	protected void onStart() {
    	
		super.onStart();
		
		getSpiceManager().execute(spiceRequestNewsCategories, "json", DurationInMillis.ALWAYS_EXPIRED, new NewsCategoriesSpiceRequestListener());
		
		Log.v("TEST", "Cat started");
		
    }
    //*/
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_view);
        
        myContext = getApplicationContext();
        
		spiceRequestNewsCategories = new SpiceRequestNewsCategories();        
        spiceRequestTechnology = new SpiceRequestTechnology(); //"technology");
        spiceRequestTopStories = new SpiceRequestTopStories();
        
        
        /*
        Intent i = getIntent();        
        newsCategories = (NewsCategories) i.getSerializableExtra("newsCategories");

        setTitle("Your News Categories");
        mGridView = (StaggeredGridView) findViewById(R.id.grid_view);

        mAdapter = new CategoryDataAdapter(this, R.layout.category_list_item, newsCategories.getCategories()); //SampleData.generateSampleData());

        mGridView.setAdapter(mAdapter);

        mGridView.setOnItemClickListener(this);
        */

    }
    
    private int positionClicked = 0;

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        Toast.makeText(this, "Item Clicked: " + position, Toast.LENGTH_SHORT).show();
        
        positionClicked = position;
        
        if(position == 0)
        {
        	
        	
        	if(getSpiceManager() == null)
        	{
        		Log.v("TEST", "Spice manager null");
        	}
        	else
        	{
        		getSpiceManager().execute(spiceRequestTechnology, "json", DurationInMillis.ALWAYS_EXPIRED, new TopStoriesSpiceRequestListener());
        	}	
        	
        	
        }
        
        if(position == 1)
        {
        	
        	
        	if(getSpiceManager() == null)
        	{
        		Log.v("TEST", "Spice manager null");
        	}
        	else
        	{
        		getSpiceManager().execute(spiceRequestTopStories, "json", DurationInMillis.ALWAYS_EXPIRED, new TopStoriesSpiceRequestListener());
        	}	
        	
        }
        
    }
    
	public final class TopStoriesSpiceRequestListener implements RequestListener<TopStories> {

		@Override
		public void onRequestFailure( SpiceException spiceException ) {
			
			Log.v("TEST", "Top Stories Fetch failure");
			
			Toast.makeText(CategoryActivity.this, "failure", Toast.LENGTH_SHORT ).show();
		}

		@Override
		public void onRequestSuccess( final TopStories result ) {
			
			Toast.makeText(CategoryActivity.this, "success", Toast.LENGTH_SHORT ).show();
			
			if(result.getNewsBites() == null)
			{
				
				Log.v("TEST", "NewsBites null");
				
				return ;
			}
			
			Log.v("TEST", "CategoryActivity topStories clicked " + result.toString());			
			Log.v("TEST", "JSON object read " + result.getNewsBites().toString());			
			Log.v("TEST", result.getNewsBites().get(0).getTitle());
			
			//5.
			//textToSpeech.speak("Hello folks, reading out news", TextToSpeech.QUEUE_ADD, null);		
			//textToSpeech.speak(result.getNewsBites().get(0).getTitle(), TextToSpeech.QUEUE_ADD, null);			
			//textToSpeech.speak(result.getNewsBites().get(0).getSummary(), TextToSpeech.QUEUE_ADD, null);
			
			//ListView listView = (ListView) findViewById(android.R.id.list);
			
			
			
			
			Intent intent = new Intent(myContext, ArticleActivity.class);
			intent.putExtra("topStories", result);
			
			Log.v("TEST", "Starting activity");
			
			startActivity(intent);
			
			//RedditChildAdapter adapter = (RedditChildAdapter) listView.getAdapter();
			//adapter.clear();
			//adapter.addAll(result.getData().getChildren());

		}
		
	}
	
	void setAdapter()
	{
		setTitle("Your News Categories");
        mGridView = (StaggeredGridView) findViewById(R.id.grid_view);
        mAdapter = new CategoryDataAdapter(this, R.layout.category_list_item, newsCategories.getCategories()); //SampleData.generateSampleData());
        mGridView.setAdapter(mAdapter);
        mGridView.setOnItemClickListener(this);
	}
	
	public final class NewsCategoriesSpiceRequestListener implements RequestListener< NewsCategories > {

		@Override
		public void onRequestFailure( SpiceException spiceException ) {
			
			
			Toast.makeText(CategoryActivity.this, "spice failure categories", Toast.LENGTH_SHORT ).show();
			Log.v("TEST", "categories fetch failure");
			
		}

		@Override
		public void onRequestSuccess( final NewsCategories result ) {
			
			Toast.makeText( CategoryActivity.this, "spice success categories", Toast.LENGTH_SHORT ).show();
			Log.v("TEST", "spice success categories");		
			Log.v("TEST", result.getCategories().get(0).getTitle());			
			
			//RedditChildAdapter adapter = (RedditChildAdapter) listView.getAdapter();
			//adapter.clear();
			//adapter.addAll(result.getData().getChildren());
			
			newsCategories = result;
			
			/*
			Intent intent = new Intent(myContext, CategoryActivity.class);
			intent.putExtra("newsCategories", result);
			
			Log.v("TEST", "Starting activity");
			
			startActivity(intent);
			*/
			
			setAdapter();

		}
		
	}


}
