package com.example.newsbites;

import java.io.Serializable;

import android.util.Log;

import com.google.api.client.util.Key;

public class ATopStory implements Serializable {

	@Key
	private String title;
	@Key
	private String authors;
	@Key
	private String source;
	@Key
	private String pub_date;
	@Key
	private String summary;
	@Key
	private String img_url;
	@Key
	private String url;
	@Key
	private int fb_cnt;
	@Key
	private int tw_cnt;
	@Key
	private int lnk_cnt;
	@Key
	private int gplus_cnt;
	@Key
	private int comments;
	
	public ATopStory()
	{
		
	}
	
	public String getTitle()
	{
		return this.title;
	}
	
	public String getAuthors()
	{
		return this.authors;
	}
	
	public String getSource()
	{
		return this.source;
	}
	
	public String getPubDate()
	{
		return this.pub_date;
	}
	
	public String getSummary()
	{
		return this.summary;
	}
	
	public String getImgUrl()
	{
		return this.img_url;
	}
	
	public String getUrl()
	{
		return this.url;
	}
	
	public int getFbCnt()
	{
		return this.fb_cnt;
	}
	
	public int getTwCnt()
	{
		return this.tw_cnt;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( title == null ? 0 : title.hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		ATopStory other = (ATopStory) obj;
		if ( title == null ) {
			if ( other.title != null ) {
				return false;
			}
		} else if ( !title.equals( other.title ) ) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		
		Log.v("TEST", "Top Story string");
		
		return "TopStories [Title=" + title + " Authors=" + authors + " Source=" + source + 
				" Date=" + pub_date + "]";
	}
	
}
