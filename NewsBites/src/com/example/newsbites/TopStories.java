package com.example.newsbites;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.api.client.util.Key;

public class TopStories implements Serializable {
	
	@Key
	private String title;
	@Key
	private ArrayList<ATopStory> newsbites;
	
	public TopStories() {
	}

	public String getTitle() {
		return this.title;
	}

	public ArrayList<ATopStory> getNewsBites() {
		
		
		return this.newsbites;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( title == null ? 0 : title.hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		TopStories other = (TopStories) obj;
		if ( title == null ) {
			if ( other.title != null ) {
				return false;
			}
		} else if ( !title.equals( other.title ) ) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "TopStories [title=" + title + "]";
	}

	/*
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub
		
		dest.writeString(title);
		dest.writeList(newsbites);
		
	}
	*/

}
