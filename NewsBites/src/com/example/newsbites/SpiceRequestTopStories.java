package com.example.newsbites;

import java.io.IOException;

import roboguice.util.temp.Ln;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.json.jackson.JacksonFactory;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

public class SpiceRequestTopStories extends GoogleHttpClientSpiceRequest {
	private String baseUrl;

    @SuppressWarnings("unchecked")
	public SpiceRequestTopStories() {
        super( TopStories.class );
        this.baseUrl = String.format( "http://54.210.247.249:8001/topstories/?format=json" );
    }

   
	@Override
    
	public TopStories loadDataFromNetwork() throws IOException {
        Ln.d( "Call web service " + baseUrl );
        HttpRequest request = getHttpRequestFactory()//
                .buildGetRequest( new GenericUrl( baseUrl ) );
        request.setParser( new JacksonFactory().createJsonObjectParser() );
        return request.execute().parseAs( getResultType() );
    }
	
}