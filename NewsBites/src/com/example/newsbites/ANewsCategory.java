package com.example.newsbites;

import java.io.Serializable;

import android.util.Log;

import com.google.api.client.util.Key;

public class ANewsCategory implements Serializable {

	@Key
	private String title;
	@Key
	private String name;
	@Key
	private int num_newsbites;
	
	//private String description;
	//private String img_url;
	
	
	public ANewsCategory()
	{
		
	}
	
	public String getTitle()
	{
		return this.title;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public int getNumNewsBites()
	{
		return this.num_newsbites;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( title == null ? 0 : title.hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		ANewsCategory other = (ANewsCategory) obj;
		if ( title == null ) {
			if ( other.title != null ) {
				return false;
			}
		} else if ( !title.equals( other.title ) ) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		
		return "Cateogories [Title=" + title + "]";
	}
	
}
