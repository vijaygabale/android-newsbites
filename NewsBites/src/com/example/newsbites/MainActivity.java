package com.example.newsbites;

import com.example.newsbites.R;
import com.example.newsbites.ui.ArticleActivity;
import com.example.newsbites.ui.CategoryActivity;
import com.octo.android.robospice.persistence.DurationInMillis;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import android.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.ListView;
import android.widget.Toast;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import java.util.Locale;

public class MainActivity extends BaseSpiceActivity{ 
//1. implements TextToSpeech.OnInitListener, TextToSpeech.OnUtteranceCompletedListener {
	

	Context myContext;
    private static final int MY_DATA_CHECK_CODE = 1234;
    
    //2.
    //private TextToSpeech textToSpeech;
    
    private SpiceRequestTopStories spiceRequestTopStories;
	private SpiceRequestNewsCategories spiceRequestNewsCategories;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		myContext = getApplicationContext();
		//Log.v("TEST", "MainActivity Started");
		
		/*
		FragmentManager fm = getFragmentManager();

		if (fm.findFragmentById(android.R.id.content) == null) {
			RedditListFragment list = new RedditListFragment();
			fm.beginTransaction().add(android.R.id.content, list).commit();
		}
		
		spiceRequestReddit = new SpiceRequestReddit( "Riak" );
		*/
		
		spiceRequestTopStories = new SpiceRequestTopStories();
		spiceRequestNewsCategories = new SpiceRequestNewsCategories();
		
		
		//3.
		/*
		// Fire off an intent to check if a TTS engine is installed
        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkIntent, MY_DATA_CHECK_CODE);
		//*/
	
	}
	
	//-----------------------------------------------------------------------
	//4.
	/*
	public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == MY_DATA_CHECK_CODE)
        {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS)
            {
                // success, create the TTS instance
                textToSpeech = new TextToSpeech(this, this);                
                Toast.makeText( MainActivity.this, "TTS available", Toast.LENGTH_SHORT ).show();              
                
            }
            else
            {
            	Toast.makeText( MainActivity.this, "TTS not available", Toast.LENGTH_SHORT ).show();
            	
                // missing data, install it
                Intent installIntent = new Intent();
                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
    }
	
	public void onInit(int status) {

	        if (status == TextToSpeech.SUCCESS) {

	            int result = textToSpeech.setLanguage(Locale.US);

	            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
	            	Log.e("TTS", "This Language is not supported");
	            } else {
	          
	            
	            	textToSpeech.speak("Hello folks, success", TextToSpeech.QUEUE_ADD, null);
	            }

	        } else {
	            Log.e("TTS", "Initilization Failed!");
	        }

	}
	  
	public void onUtteranceCompleted(String uttId) {
		    
	}
	
	
	 @Override
	 public void onDestroy()
	 {
	        
		 	// Don't forget to shutdown!
	        
		 	if (textToSpeech != null)
	        {
	            textToSpeech.stop();
	            textToSpeech.shutdown();
	        }
	        super.onDestroy();
	 }
	
	*/
	
	//-----------------------------------------------------------------------

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onStart() {
		super.onStart();

		setProgressBarIndeterminate( false );
		setProgressBarVisibility( true );
		
		getSpiceManager().execute(spiceRequestNewsCategories, "json", DurationInMillis.ONE_MINUTE, new NewsCategoriesSpiceRequestListener());
		
		//getSpiceManager().execute(spiceRequestTopStories, "json", DurationInMillis.ONE_MINUTE, new TopStoriesSpiceRequestListener() );
		
	}

	// ============================================================================================
	// INNER CLASSES
	// ============================================================================================
	
	public final class TopStoriesSpiceRequestListener implements RequestListener< TopStories > {

		@Override
		public void onRequestFailure( SpiceException spiceException ) {
			
			Log.v("TEST", "Top Stories Fetch failure");
			
			Toast.makeText( MainActivity.this, "failure", Toast.LENGTH_SHORT ).show();
		}

		@Override
		public void onRequestSuccess( final TopStories result ) {
			
			Toast.makeText( MainActivity.this, "success", Toast.LENGTH_SHORT ).show();
			Log.v("TEST", result.toString());
			
			Log.v("TEST", "JSON object read " + result.getNewsBites().toString());
			
			Log.v("TEST", result.getNewsBites().get(0).getTitle());
			
			//5.
			//textToSpeech.speak("Hello folks, reading out news", TextToSpeech.QUEUE_ADD, null);		
			//textToSpeech.speak(result.getNewsBites().get(0).getTitle(), TextToSpeech.QUEUE_ADD, null);			
			//textToSpeech.speak(result.getNewsBites().get(0).getSummary(), TextToSpeech.QUEUE_ADD, null);
			
			//ListView listView = (ListView) findViewById(android.R.id.list);
			
			
			Intent intent = new Intent(myContext, ArticleActivity.class);
			intent.putExtra("topStories", result);
			
			Log.v("TEST", "Starting activity");
			
			startActivity(intent);
			
			//RedditChildAdapter adapter = (RedditChildAdapter) listView.getAdapter();
			//adapter.clear();
			//adapter.addAll(result.getData().getChildren());

		}
		
	}
	

	public final class NewsCategoriesSpiceRequestListener implements RequestListener< NewsCategories > {

		@Override
		public void onRequestFailure( SpiceException spiceException ) {
			
			
			Toast.makeText( MainActivity.this, "spice failure categories", Toast.LENGTH_SHORT ).show();
			Log.v("TEST", "categories fetch failure");
			
		}

		@Override
		public void onRequestSuccess( final NewsCategories result ) {
			
			Toast.makeText( MainActivity.this, "spice success categories", Toast.LENGTH_SHORT ).show();
			Log.v("TEST", "spice success categories");		
			Log.v("TEST", result.getCategories().get(0).getTitle());			
			
			//RedditChildAdapter adapter = (RedditChildAdapter) listView.getAdapter();
			//adapter.clear();
			//adapter.addAll(result.getData().getChildren());
			
			Intent intent = new Intent(myContext, CategoryActivity.class);
			intent.putExtra("newsCategories", result);
			
			Log.v("TEST", "Starting activity");
			
			startActivity(intent);
			

		}
		
	}
	
	
}
