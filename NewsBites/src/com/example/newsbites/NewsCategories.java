package com.example.newsbites;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.api.client.util.Key;

public class NewsCategories implements Serializable {

	@Key
	private ArrayList<ANewsCategory> categories;
	
	public NewsCategories()
	{
		
	}
	
	public ArrayList<ANewsCategory> getCategories()
	{
		return this.categories;
	}
	
}
